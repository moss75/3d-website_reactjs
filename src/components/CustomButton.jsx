import React from 'react';
import state from '../store';
import {useSnapshot} from "valtio";
import {getContrastingColor} from "../config/helpers.js";

const CustomButton = ({type, title, customeStyles, handleClick}) => {
    const snap = useSnapshot(state);
    const generateStyle = (type) => {
        if(type==='filler') {
            return {
                backgroundColor: snap.color,
                color: getContrastingColor(snap.color)
            }
        } else if (type=== "outline"){
            return {
                border: `1px solid ${snap.color}`
            }
        }
    }

    return (
        <button
            className={`px-2 py-1.5 flex-1 rounder-md ${customeStyles}`}
            style={generateStyle(type)}
            onClick={handleClick}
        >
            {title}

        </button>
    )
}

export default CustomButton