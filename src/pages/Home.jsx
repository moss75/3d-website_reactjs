import React from 'react'
import { motion, AnimatePresence } from "framer-motion";
import { useSnapshot } from "valtio";
import {headContainerAnimation, headContentAnimation, headTextAnimation, slideAnimation} from '../config/motion.js'
import CustomButton from "../components/CustomButton.jsx";
import state from '../store/index.js'
const Home = () => {
    const snap = useSnapshot(state)
    return (
        <AnimatePresence>
            {snap.intro && (
                <motion.section className="home" {...slideAnimation('left')}>
                    <motion.header>
                        <img src="../../public/threejs.png" alt="logo" className="w-8 h-8 object-contain"/>
                    </motion.header>
                    <motion.div className="home-content" {...headContainerAnimation}>
                        <motion.div {...headContainerAnimation}>
                            <h1 className="head-text">
                                LET'S <br className="x11:block hidden" /> GO
                            </h1>
                        </motion.div>
                        <motion.div  className="flex flex-col gap-5" {...headContentAnimation}>
                            <p className="max-w-md font-normal text-gray-600 ">
                                Créez votre chemise unique et exclusive avec notre tout nouvel outil de personnalisation 3D. <strong>Libérez votre imagination </strong>
                                et définissez votre propre style.
                            </p>
                            <CustomButton
                                type="filler"
                                title="Custom"
                                handleClick={() => state.intro = false}
                                customeStyles="w-fit pw-4 py-2.5 font-bold text-sm"
                            />
                        </motion.div>
                    </motion.div>

                </motion.section>
            )}
        </AnimatePresence>
    )
}

export default Home