import { proxy } from "valtio";

const state = proxy({
    intro: true,
    color: '#4e75d2',
    isLogoTexture: true,
    isFullTexture: false,
    logoDecal: './threejs.png',
    fulldecal: './threejs.png'
})

export default state